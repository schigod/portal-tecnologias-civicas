const most = [
    {name: "Alto Feliz", total_tec: 9},
    {name: "Bom Princípio", total_tec: 7},
    {name: "Brochier", total_tec: 10},
    {name: "Feliz", total_tec: 18},
    {name: "Harmonia", total_tec: 7},
    {name: "Montenegro", total_tec: 15},
    {name: "Pareci Novo", total_tec: 10},
    {name: "S.P. da Serra", total_tec: 10},
    {name: "Vale Real", total_tec: 7}
];
const less = [
    {name: "S. Vendelino", total_tec: 3},
    {name: "Maratá", total_tec: 6},
    {name: "S.S. do Caí", total_tec: 4},
    {name: "S.J. do H.", total_tec: 5},
    {name: "Portão", total_tec: 5},
    {name: "Linha Nova", total_tec: 5},
    {name: "C. de Santana", total_tec: 5},
    {name: "Barão", total_tec: 5},
    {name: "Tupandi", total_tec: 6},
    {name: "Salvador do S.", total_tec: 6},
    {name: "S.J. do Sul", total_tec: 6}

];

const AF = [
    {name: "Transparência", value: 6},
    {name: "Interação", value: 3}
];

const Bar = [
    {name: "Transparência", value: 4},
    {name: "Interação", value: 1}
];
const BP = [
    {name: "Transparência", value: 5},
    {name: "Interação", value: 2}
];
const BRO = [
    {name: "Transparência", value: 9},
    {name: "Interação", value: 1}
];
const CDE = [
    {name: "Transparência", value: 4},
    {name: "Interação", value: 1}
];
const FE = [
    {name: "Transparência", value: 13},
    {name: "Interação", value: 5}
];
const HAR = [
    {name: "Transparência", value: 6},
    {name: "Interação", value: 1}
];
const LIN = [
    {name: "Transparência", value: 4},
    {name: "Interação", value: 1}
];
const MAR = [
    {name: "Transparência", value: 4},
    {name: "Interação", value: 2}
];
const MON = [
    {name: "Transparência", value: 13},
    {name: "Interação", value: 2}
];
const PAR = [
    {name: "Transparência", value: 8},
    {name: "Interação", value: 2}
];
const POR = [
    {name: "Transparência", value: 4},
    {name: "Interação", value: 1}
];
const SHOR = [
    {name: "Transparência", value: 4},
    {name: "Interação", value: 1}
];
const SSUL = [
    {name: "Transparência", value: 4},
    {name: "Interação", value: 2}
];
const SPS = [
    {name: "Transparência", value: 8},
    {name: "Interação", value: 2}
];
const SSC = [
    {name: "Transparência", value: 3},
    {name: "Interação", value: 1}
];
const SVEN = [
    {name: "Transparência", value: 2},
    {name: "Interação", value: 1}
];
const SALV = [
    {name: "Transparência", value: 5},
    {name: "Interação", value: 1}
];
const TUP = [
    {name: "Transparência", value: 3},
    {name: "Interação", value: 3}
];
const VAL = [
    {name: "Transparência", value: 3},
    {name: "Interação", value: 4}
];

var typeGraphic = "#d3-container-most-tec";

widthScreen = window.screen.width;
heightScreen = window.screen.height;
var fontsizeX;

if(widthScreen > 1200){
    var width = widthScreen;
    var height = 720;
    fontsizeX = "18px";
}else if(widthScreen > 800){
    var width = widthScreen;
    var height = 620;
    fontsizeX = "14px";
}else if(widthScreen > 400){
    var width = widthScreen;
    var height = 350;
    fontsizeX = "8px";
}else{
    var width = widthScreen;
    var height = heightScreen - 80;
    fontsizeX = "6px";
}


const margin = {top: 50, bottom: 50, left:50, right: 50};

var svg = d3.select(typeGraphic)
    .append("svg")
        .attr("height", height - margin.top - margin.bottom)
        .attr("width", width - margin.left - margin.right)
        .attr("viewBox", [0, 0, width, height]);


function generateTotalTec(table){
    d3.select("svg").remove();

    var svg = d3.select(typeGraphic)
    .append("svg")
        .attr("height", height - margin.top - margin.bottom)
        .attr("width", width - margin.left - margin.right)
        .attr("viewBox", [0, 0, width, height]);



    var x = d3.scaleBand()
    .domain(d3.range(table.length))
    .range([margin.left, width - margin.right])
    .padding(0.1);

var y = d3.scaleLinear()
    .domain([0, 20])
    .range([height - margin.bottom, margin.top]);

svg
    .append("g")
    .attr("fill", "royalblue")
    .selectAll("rect")
    .data(table.sort((a, b) => d3.descending(a. total_tec, b. total_tec)))
    .join("rect")
        .attr("x", (d, i) => x(i))
        .attr("y", d => y(d. total_tec))
        .attr("height", d => y(0) - y(d. total_tec))
        .attr("width", x.bandwidth())
        .attr("class", "rectangle")

function xAxis(g){
    g.attr("transform", `translate(0, ${height - margin.bottom})`)
    .call(d3.axisBottom(x).tickFormat(i => table[i].name))
    .attr("font-size", fontsizeX)
}
function yAxis(g){
    g.attr("transform", `translate(${margin.left}, 0)`)
    .call(d3.axisLeft(y).ticks(null, table.format))
    .attr("font-size", "20px")
}

svg.append("g").call(yAxis);
svg.append("g").call(xAxis);
svg.node();
}

function generateByCity(city){
    d3.select("svg").remove();

    var svg = d3.select(typeGraphic)
    .append("svg")
        .attr("height", height - margin.top - margin.bottom)
        .attr("width", width - margin.left - margin.right)
        .attr("viewBox", [0, 0, width, height]);



    var x = d3.scaleBand()
    .domain(d3.range(city.length)) //////COLUNAS
    .range([margin.left, width - margin.right])
    .padding(0.1);

    var y = d3.scaleLinear()
        .domain([0, 13])
        .range([height - margin.bottom, margin.top]);

    svg
        .append("g")
        .attr("fill", "green")
        .selectAll("rect")
        .data(city) 
        .join("rect")
            .attr("x", (d, i) => x(i))
            .attr("y", d => y(d. value))
            .attr("height", d => y(0) - y(d. value))
            .attr("width", x.bandwidth())
            .attr("class", "rectangle")

    function xAxis(g){
        g.attr("transform", `translate(0, ${height - margin.bottom})`)
        .call(d3.axisBottom(x).tickFormat(i => city[i].name))
        .attr("font-size", "20px")
    }
    function yAxis(g){
        g.attr("transform", `translate(${margin.left}, 0)`)
        .call(d3.axisLeft(y).ticks(null, city.format))
        .attr("font-size", "20px")
    }

    svg.append("g").call(yAxis);
    svg.append("g").call(xAxis);
    svg.node();
}

const messageTypeGraphic = document.getElementById("typeGraphic");


function generateMessageByCity(city){
    messageTypeGraphic.innerHTML = `Total de tecnologias cívicas de transparência e de interação de ${city}`;
}
var button = document.getElementById("generate");
var optionSelected = document.querySelector("#graphics");

button.addEventListener("click", function(x){
    switch(optionSelected.value){
        case "total1":
            generateTotalTec(most);
            messageTypeGraphic.innerHTML = "Cidades com mais tecnologias cívicas";
            break
        case "total2":
            generateTotalTec(less);
            messageTypeGraphic.innerHTML = "Cidades com menos tecnologias cívicas";
            break
        case "altoFeliz":
            generateByCity(AF);
            generateMessageByCity("Alto Feliz");
            break
        case "barao":
            generateByCity(Bar);
            generateMessageByCity("Barão");
            break
        case "bp":
            generateByCity(BP);
            generateMessageByCity("Bom Princípio");
            break
        case "bro":
            generateByCity(BRO);
            generateMessageByCity("Brochier");
            break
        case "csant":
            generateByCity(CDE);
            generateMessageByCity("Capela de Santana");
            break
        case "feliz":
            generateByCity(FE);
            generateMessageByCity("Feliz");
            break
        case "harm":
            generateByCity(HAR);
            generateMessageByCity("Harmonia");
            break
        case "lnov":
            generateByCity(LIN);
            generateMessageByCity("Linha Nova");
            break
        case "marata":
            generateByCity(MAR);
            generateMessageByCity("Maratá");
            break
        case "mont":
            generateByCity(MON);
            generateMessageByCity("Montenegro");
            break
        case "pnovo":
            generateByCity(PAR);
            generateMessageByCity("Pareci Novo");
            break
        case "port":
            generateByCity(POR);
            generateMessageByCity("Portão");
            break
        case "sjhor":
            generateByCity(SHOR);
            generateMessageByCity("São José do Hortêncio");
            break
        case "sjsul":
            generateByCity(SSUL);
            generateMessageByCity("São José do Sul");
            break
        case "spserra":
            generateByCity(SPS);
            generateMessageByCity("São Pedro da Serra");
            break
        case "sscai":
            generateByCity(SSC);
            generateMessageByCity("São Sebastião do Caí");
            break
        case "svend":
            generateByCity(SVEN);
            generateMessageByCity("São Vendelino");
            break
        case "salvador":
            generateByCity(SALV);
            generateMessageByCity("Salvador do Sul");
            break
        case "tupandi":
            generateByCity(TUP);
            generateMessageByCity("Tupandi");
            break
        case "vreal":
            generateByCity(VAL);
            generateMessageByCity("Vale Real");
            break
    }   
});


